class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.integer :amount
      t.references :category, index: true

      t.timestamps
    end
  end
end
