class CostsController < AuthenticatedController
  before_action :set_cost, only: [:show, :edit, :update, :destroy]

  # GET /costs
  # GET /costs.json
  def index
    @costs = Cost.all
  end

  # GET /costs/1
  # GET /costs/1.json
  def show
    @cost = Cost.find_by_id(params[:id])
  end

  # GET /costs/new
  def new
    @cost = Cost.new
    @categories = Category.all
  end

  # GET /costs/1/edit
  def edit
    @cost = Cost.find_by_id(params[:id])
    @categories = Category.all
  end

  # POST /costs
  # POST /costs.json
  def create
    # @cost = @categories.expenses.new(cost_params)
    @cost = Cost.new(cost_params) do |cost|
      cost.category = Category.find_by_id(params[:category])
    end

    respond_to do |format|
      if @cost.save
        format.html { redirect_to @cost, notice: 'Cost was successfully added.' }
        format.json { render :show, status: :created, location: @cost }
        format.js
      else
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /costs/1
  # PATCH/PUT /costs/1.json
  def update
    @cost = Cost.update(cost_params) do |cost|
      cost.category = Category.find_by_id(params[:category])
    end

    respond_to do |format|
      if @cost.persisted?
        format.html { redirect_to @cost, notice: 'Cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @cost }
      else
        format.html { render :edit }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /costs/1
  # DELETE /costs/1.json
  def destroy
    @cost.destroy
    respond_to do |format|
      format.html { redirect_to costs_url, notice: 'Cost was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost
      @cost = Cost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_params
      params.require(:cost).permit(:amount, :category)
    end
end
