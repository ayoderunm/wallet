module ApplicationHelper
  def new_category_button
    link_to "New Category", new_category_path, class: 'btn btn-primary'
  end
end
