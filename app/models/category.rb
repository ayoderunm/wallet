class Category < ActiveRecord::Base
  has_many :costs
  has_many :incomes
end
