class Overview
  attr_reader :costs,  :expenses
  
  def initialize(costs, expenses)
    @costs = costs
    @expenses = expenses
  end

  def costs_total
    cost_total = 0
    @costs.each { |cost| cost_total += cost.amount }
    return cost_total
  end

  def most_used_categories_for_costs
    cost_categories = @costs.map(&:category)
  end
end
